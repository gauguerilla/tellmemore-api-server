# TELLMEMORE
## Server API

API server based on nodejs express

API docs and endpoints at: https://documenter.getpostman.com/view/5276452/SzmjzaST

## dependencies

you need nodejs installed, an SQL server (tested with MariaDB) running.

modify the file server.config.example by renaming it server.config and provide DB credentials.

then create the tables `panels` and `speakers` using `createPanels.sql` and `createSpeakers.sql`

then install nodejs package:


## installation
```bash
npm i
```

## run
```bash
npm run start
```
or
```bash
node app.js
```
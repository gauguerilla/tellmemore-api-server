/*
 API doc at: https://documenter.getpostman.com/view/5276452/SzmjzaST
*/


const express = require('express')
const app = express()
const {promisify} = require("util"); 
const mysql = require('mysql');
const randomstring = require("randomstring");
const fs = require("fs"); 
const readFile = promisify(fs.readFile);

const config = JSON.parse(fs.readFileSync("server.config"));
console.log("config loaded",config);


app.use(express.json()) // for parsing application/json


let lastSQLConnectionAttempt = Date.now();

var db;

connectToDB();

function connectToDB(){
  db = mysql.createConnection({
    host     : config.dbHost,
    user     : config.dbAdmin,
    password : config.dbPassword,
    database : config.dbName
  });

  lastSQLConnectionAttempt = Date.now();
  db.connect(function(err) {
    if (err) {
      console.error('error connecting: ' + err.stack);
      return handleSQLConnectionError(err);
    }
  
    console.log('connected as id ' + db.threadId)
    })

  db.on("error",(err)=>{
    handleSQLConnectionError(err);
  })
}

function handleSQLConnectionError(err){
  console.log("SQL connection threw an error:",err);

  if (Date.now() - lastSQLConnectionAttempt > 5000)
  {
    console.log("###########","will try to reconnect");
    connectToDB();
  }
  else
  {
    setTimeout(() => {
      console.log("###########","will try to reconnect");
      connectToDB();
    }, 6000);
  }
}

db.query(' SELECT * FROM panels WHERE panelID = "test" ', function (error, results, fields) {
  if (error) handleSQLConnectionError(error)//throw error;
  if (results)
  {
    console.log('table is: ', results);
  }
});

///////////////////
/*    ROUTES     */
///////////////////

// startpage
app.get('/', function (req, res) {
  res.send('startpage')
})

/**
 * returns all active panels as json
 * 
 * TODO: add middleware to check readrights! 
 */
app.get('/panel', async function (req, res) {
  let allpanels = await getAllPanels();
  for (i in allpanels){
    allpanels[i].AuthToken = "XXXXXX";
  }
  return res.send(JSON.stringify(allpanels))
})


/**
 * create new panel on this instance
 */
app.post('/panel', async function (req, res) {

  let body = req.body;
  
  if (!body.maxTime || typeof body.maxTime !== "number"){
    res.statusCode = 404;
    return res.send(`{"reason":"maxTime not defined."}`)
  }

  let created = await createNewPanel(req,res,body).catch((err)=>{console.log(err); return;});
  console.log("created",created);

  let speakers = [];
  if (body.speakers && body.speakers.length > 0){
    console.log("will also create speakers", body.speakers, "using panelID:",created.panelID);
    
    let arrayOfSpeakerPromises = [];
    for (s of body.speakers){
      s.panelID = created.panelID;
      arrayOfSpeakerPromises.push(createSpeaker(s)); // CONTINUE HERE: add panelID automatically. 
    }
    speakers = await Promise.all(arrayOfSpeakerPromises)
      .catch((err)=>{console.log("issue while creating speaker",err);});
    speakers = JSON.stringify(speakers)
  }
  
  if (!created){return}

  let output = JSON.stringify(created);
  return res.send(`{"res":"ok","createdPanels":${output}, "createdSpeakers":${speakers}}`)
})

/**
 * returns JSON with all tablecontent about the panel based on panelID url param
 * will omit Auth infos tho
 */
app.get('/panel/:panelID', async function (req, res) {

  if (!req.params.panelID){
    return res.sendStatus(404)
  }
  
  let panelinfo = await getPanelByID(req.params.panelID);
  
  if (panelinfo.length < 1){
    return res.send(`{"reason":"unknown panel ID. please provide valid panel ID!"}`)
  }

  panelinfo[0].AuthToken = "XXXX";
  
  panelinfo[0].speakers = await getAllSpeakersByPanel(req.params.panelID).catch((err)=>{
    console.log("error while fetching speakers to panel",err);
    return res.sendStatus(500);
  });
  
  return res.send(`${JSON.stringify(panelinfo[0])}`)
})

/**
 * update a panel with panel ID 
 * specifying the panels options with the post body. if speakers are defined they will be created or updated
 */
app.put('/panel/:panelID', async function (req, res) {

  let panelconfig = req.body;

  if (!req.params.panelID){
    res.statusCode = 404;
    return res.send(`{"reason":"please provide valid panel ID"}`)
  }
  
  let panelinfo = await getPanelByID(req.params.panelID);
  
  if (panelinfo.length < 1){
    return res.send(`{"reason":"please provide valid panel ID"}`)
  }

  // exists. therefore update
  let updated = await updatePanel(req, res,panelinfo[0], panelconfig).catch((err)=>{console.log("ERROR UPDATING"/*,err*/);})

  if (!updated){return res.sendStatus(500);}

  let speakers = [];
  if (panelconfig.speakers && panelconfig.speakers.length > 0){
    console.log("will also update speakers", panelconfig.speakers, "using panelID:",updated.panelID);
    
    let arrayOfSpeakerPromises = [];
    for (s of panelconfig.speakers){
      s.panelID = updated.panelID;
      arrayOfSpeakerPromises.push(createOrUpdateSpeaker(s)); 
    }
    speakers = await Promise.all(arrayOfSpeakerPromises)
      .catch((err)=>{
        console.log("issue while creating speaker",err);
        return res.sendStatus(500);
    });
    speakers = JSON.stringify(speakers)
  }
  if(typeof speakers !== "string"){speakers = "{}"}

  return res.send(`{"res":"ok","updatedPanels":${JSON.stringify(updated)}, "updatedSpeakers":${speakers}}`)
  
})

/**
 * delete a panel with panel ID 
 * needs to provide AuthToken of panel to do so
 */
app.delete('/panel/:panelID', async function (req, res) {

  let panelconfig = req.body;

  if (!req.params.panelID){
    res.statusCode = 404;
    return res.send(`{"reason":"please provide valid panel ID"}`)
  }
  
  let panelinfo = await getPanelByID(req.params.panelID);
  
  if (panelinfo.length < 1){
    return res.send(`{"reason":"please provide valid panel ID"}`)
  }

  if (panelinfo[0].AuthToken !== panelconfig.AuthToken){
    res.statusCode = 401;
    return res.send(`{"reason":"not authorized. please provide AuthToken in body"}`)
  }

  // exists and auth ok. therefore delete
  // first delete all associated speakers:
  let deletedSpeakers = await deleteAllSpeakersFromPanel(req.params.panelID).catch((err)=>{console.log("ERROR DELETING USERS",err)});
  if (!deletedSpeakers){res.statusCode = 500; return res.send(`{"reason":"Database error while user Deletion"}`)}

  let deleted = await deletePanel(req.params.panelID).catch((err)=>{console.log("ERROR DELETING",err)})
  console.log("deleted:",deleted);
  
  if (!deleted){res.statusCode = 500; return res.send(`{"reason":"Database error while Deletion"}`)}

  return res.send(`{"ok":true, "deleted":${req.params.panelID}}`)
})


//// SPEAKER ////

// speaker creation: see panel create.

// CONTINUE HERE: 
// TODO:
// - speaker Update (inside of panel update and/or inside its own endpoint)
// - delete Speaker (on panel update)
// - panel reset endpoint
// - optional: speaker reset endpoint
// - optional: different type of timestamp return

app.get('/panel/:panelID/speaker/:speakerID', async function (req, res) {

  let speakers = await getSpeakerByID(req.params.speakerID);

  if (speakers.length < 1){
    res.statusCode = 403;
    return res.send(`{"reason":"unknown speaker ID. please provide valid speaker ID!"}`)
  }

  return res.send(`${JSON.stringify(speakers[0])}`)

})

app.post('/panel/:panelID/speaker/:speakerID/timer/start', async function (req, res) {

  // startTimer by setting Unix Timestamp
  let timestamp = new Date().getTime();
  let speaker = await getSpeakerByID(req.params.speakerID)
  .catch((err)=>{console.log("error while starting timestamp: speaker fetching", req.params.speakerID,err)});

  console.log("SPEAKER:", speaker);
  
  if(!speaker || speaker.length < 1){res.statusCode=403; return res.send(`{"reason":"invalid speakerID"}`)}

  // write timestamp into DB:
  await setTimerOnSpeaker(req.params.speakerID,timestamp)
  .catch((err)=>{console.log("error while starting timestamp", req.params.speakerID,err);
  
  res.statusCode = 500;
  res.send(`{"reason":"error setting timestamp"}`)
  })

  // return success
  return res.send(`{"ok":true,"speaker":${req.params.speakerID},"startTimeStamp":${timestamp}}`);
})

app.post('/panel/:panelID/speaker/:speakerID/add_time', async function (req, res) {

  let gift = req.body.gift;
  if (!gift){
    res.statusCode = 403;
    res.send(`{"reason":"gift field must be specified in order to gift time"}`)
  }

  let speaker = await getSpeakerByID(req.params.speakerID)
  .catch((err)=>{console.log("error while starting timestamp: speaker fetching", req.params.speakerID,err)});

  console.log("SPEAKER:", speaker);
  
  if(!speaker || speaker.length < 1){res.statusCode=403; return res.send(`{"reason":"invalid speakerID"}`)}

  // write timestamp into DB:
  await addTimeToSpeaker(req.params.speakerID,gift)
  .catch((err)=>{console.log("error while adding time", req.params.speakerID,err);
  
  res.statusCode = 500;
  res.send(`{"reason":"error adding time"}`)
  })

  // return success
  return res.send(`{"ok":true}`);
})


// switch openToDonations on panel
app.put('/panel/:panelID/time_donations_open/:state?', async function (req, res) {

  if (!req.body.AuthToken){
    res.statusCode = 401;
    return res.send(`{"reason":"no valid AuthToken provided"}`)
  }

  let panelinfo = await getPanelByID(req.params.panelID);
  
  if (panelinfo.length < 1){
    return res.send(`{"reason":"please provide valid panel ID"}`)
  }

  if (panelinfo[0].AuthToken !== req.body.AuthToken){
    res.statusCode = 401;
    return res.send(`{"reason":"not authorized. please provide AuthToken in body"}`)
  }

  let state = req.params.state;
  
  if (req.body.state !== undefined){
    console.log("state",state);
    state = req.body.state;
  }

  console.log(req.body.state, typeof req.body.state)

  if (typeof state === "undefined"){res.statusCode = 403; return res.send(`{"reason":"no state at all"}`)}

  if (typeof state !== "boolean"){if (state && state.includes("false")){state = false}else{state = true}}
  
  // write into DB:
  await switchTimeDonations(req.params.panelID,state)
  .catch((err)=>{console.log("error while switching time donations for panelID:", req.params.panelID,err);
  
  res.statusCode = 500;
  return res.send(`{"reason":"error switching timedonations"}`)
  })

  // return success
  return res.send(`{"ok":true}`);
})

///////////////////
/* SQL FUNCTIONS */
///////////////////

function getAllSpeakersByPanel(panelID){
  return new Promise(function(resolve, reject) {
    db.query(` SELECT * FROM speakers
    WHERE panelID = ?`,[panelID], function (err, data) {
      return (err ? reject(err) : resolve(data));
    });
  });
}

function switchTimeDonations(panelID, state){
  return new Promise(function(resolve, reject) {
    db.query(` UPDATE panels
    SET openToDonations = ?
    WHERE panelID = ?`,[state, panelID], function (err, data) {
      return (err ? reject(err) : resolve(data));
    });
  });
}

function setTimerOnSpeaker(speakerID, timestamp){
  return new Promise(function(resolve, reject) {
    db.query(` UPDATE speakers
    SET startTimeStamp = ?
    WHERE speakerID = ?`,[timestamp, speakerID], function (err, data) {
      return (err ? reject(err) : resolve(data));
    });
  });
}

function addTimeToSpeaker(speakerID, gift){
  return new Promise(function(resolve, reject) {
    db.query(` UPDATE speakers
    SET giftsReceived = giftsReceived + ?
    WHERE speakerID = ?`,[gift, speakerID], function (err, data) {
      return (err ? reject(err) : resolve(data));
    });
  });
}

function getAllPanels(){
  return new Promise(function(resolve, reject) {
    db.query(' SELECT * FROM panels', function (err, data) {
      return (err ? reject(err) : resolve(data));
    });
  });

}

function getPanelByID(id){
  return new Promise(function(resolve, reject) {
    db.query(' SELECT * FROM panels WHERE panelID = ?',[id], function (err, data) {
      return (err ? reject(err) : resolve(data));
    });
  });

}

function getSpeakerByID(id){
  return new Promise(function(resolve, reject) {
    db.query(' SELECT * FROM speakers WHERE speakerID = ?',[id], function (err, data) {
      return (err ? reject(err) : resolve(data));
    });
  });

}

function updatePanel(req,res,samePanel,panelconfig){
  return new Promise(function(resolve, reject) {
    if (!panelconfig.AuthToken){
      res.statusCode = 403;
      reject(res.send(`{"reason":"AuthToken MUST be provided"}`))
    }

    // check if Auth fits
    if (samePanel.AuthToken !== panelconfig.AuthToken)
    { 
      reject((res.sendStatus(401)));
    }

    // merge old panel with new panelconfig
    Object.assign(samePanel,panelconfig);


    db.query( `UPDATE panels SET panelID = ?, maxTime = ?, numberOfGifts = ?, durationPerGift = ?, multipleGiftsAllowed = ?, openToDonations = ?
     WHERE panelID = ?;`,[samePanel.panelID, samePanel.maxTime, samePanel.numberOfGifts, 
              samePanel.durationPerGift,samePanel.multipleGiftsAllowed,
              samePanel.openToDonations, samePanel.panelID], function (err, data) {
                //console.log(data);
              return (err ? reject(err) : resolve(samePanel));
    });
  });
}

async function createNewPanel(req,res,panelconfig){

  return new Promise(async function(resolve, reject) {

    if (panelconfig.panelID === null || panelconfig.panelID === undefined){
      panelconfig.panelID = randomstring.generate(5);
    }
    else
    {
      // check if panel with same ID already exists:
      let samePanel = await getPanelByID(panelconfig.panelID);
      console.log("samepanel", samePanel);
      
      if (!samePanel || samePanel.length > 0)
      {
        res.statusCode = 403;
        res.send(`{"reason":"panelID exists already."}`)
        reject("error 403");
      }
    }

    let randomAuthToken = randomstring.generate(5)

    panelconfig = setDefaultValuesPanelConfig(panelconfig);
    panelconfig.AuthToken = randomAuthToken;

    db.query( `INSERT INTO panels (panelID, AuthToken, maxTime, numberOfGifts, durationPerGift, multipleGiftsAllowed, openToDonations) 
              VALUES (?, ?, ?, ?, ?, ?, ?);`,[panelconfig.panelID,randomAuthToken,panelconfig.maxTime, panelconfig.numberOfGifts, 
              panelconfig.durationPerGift,panelconfig.multipleGiftsAllowed,
              panelconfig.openToDonations], function (err, data) {
                //console.log(data);
                
              return (err ? reject(err) : resolve(panelconfig));
    });
  });
}

function deleteAllSpeakersFromPanel(panelID){
  return new Promise(function(resolve, reject) {
    db.query(' DELETE FROM speakers WHERE panelID = ?',[panelID], function (err, data) {
      return (err ? reject(err) : resolve(data));
    });
  });
}

function deletePanel(panelID){
  return new Promise(function(resolve, reject) {
    db.query(' DELETE FROM panels WHERE panelID = ?',[panelID], function (err, data) {
      return (err ? reject(err) : resolve(data));
    });
  });
}

async function createOrUpdateSpeaker(speakerconfig){
  console.log(speakerconfig);
  
  return new Promise(async function(resolve, reject) {

    if (!speakerconfig.panelID){
      console.log("attempt to create speaker without any panels associated. Will reject"); // SQL does this but its nice to have human friendly error messages
      reject("error 403; speaker needs to be associated with panel");
    }

    if (speakerconfig.speakerID === null || speakerconfig.speakerID === undefined){
      speakerconfig.speakerID = randomstring.generate(5);
    }
    else
    {
      // check if speaker with same ID already exists:
      let sameSpeaker = await getSpeakerByID(speakerconfig.speakerID);
      console.log("sameSpeaker", sameSpeaker);
      
      if (!sameSpeaker || sameSpeaker.length > 0)
      {
        console.log("UPDATE!!!!");
        sameSpeaker[0] = Object.assign(sameSpeaker[0],speakerconfig);// merge original with update
        console.log("updated sameSpeaker",sameSpeaker[0]);
        
        //reject("error 403; speakerID exists already");
        return new Promise(function(resolve2, reject2){
          return db.query( `UPDATE speakers SET name = ?, color = ?, startTimeStamp = ?, giftsReceived = ? 
              WHERE speakerID = ?;`,[sameSpeaker[0].name,sameSpeaker[0].color, sameSpeaker[0].startTimeStamp,
              sameSpeaker[0].giftsReceived,sameSpeaker[0].speakerID,], function (err, data) {
                console.log(data);
                
              return (err ? reject(err) : resolve(sameSpeaker));
          })
        });
      }
    }

    speakerconfig = setDefaultValuesSpeakerConfig(speakerconfig);

    db.query( `INSERT INTO speakers (speakerID, name, color, startTimeStamp, panelID, giftsReceived) 
              VALUES (?, ?, ?, ?, ?,?);`,[speakerconfig.speakerID,speakerconfig.name,speakerconfig.color, speakerconfig.startTimeStamp,
              speakerconfig.panelID, speakerconfig.giftsReceived], function (err, data) {
                //console.log(data);
                
              return (err ? reject(err) : resolve(speakerconfig));
    });
  });
}

///////////////////////////////
/*      HELPER FUNCTIONS     */
///////////////////////////////

function setDefaultValuesPanelConfig(conf){
  if (!conf.numberOfGifts){ conf.numberOfGifts = 1 }
  if (!conf.durationPerGift){ conf.durationPerGift = 1 }
  if (!conf.multipleGiftsAllowed){ conf.multipleGiftsAllowed = true }
  if (!conf.openToDonations){ conf.openToDonations = true }

  return conf
}

function setDefaultValuesSpeakerConfig(conf){
  if (!conf.name){ conf.name = randomstring.generate(5); }
  if (!conf.color){ conf.color = randomHexColor() }
  if (!conf.giftsReceived){ conf.giftsReceived = 0 }
  return conf
}

function randomHexColor(){
  return `#${Math.random().toString(16).slice(2,8).slice(-6)}`;
}

///////////////////
/*      MAIN     */
///////////////////

app.listen(config.port, () => console.log(`
Example app listening at http://localhost:${config.port}
`))
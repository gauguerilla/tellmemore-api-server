CREATE TABLE speakers (
    speakerID varchar(255) NOT NULL,
    name varchar(255) NOT NULL,
    color varchar(255) NOT NULL,
    startTimeStamp varchar(255),
    giftsReceived int,
    panelID varchar(255),
    FOREIGN KEY (panelID) REFERENCES panels(panelID),
    UNIQUE (speakerID)
);
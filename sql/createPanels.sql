CREATE TABLE panels (
    panelID varchar(255) NOT NULL,
    AuthToken varchar(255),
    maxTime int NOT NULL,
    numberOfGifts int NOT NULL DEFAULT 1,
    durationPerGift int NOT NULL DEFAULT 30,
    multipleGiftsAllowed bool NOT NULL DEFAULT true,
    openToDonations bool NOT NULL DEFAULT false,
    UNIQUE (panelID)
);